# scratch3_timer

## CREATE A SIMPLE TIMER IN SCRATCH 3.0

How to create a timer in Scratch 3.0

One of the useful things in Scratch, for example, a game is a timer.  Giving a user a set amount of time to complete a task or time how long it takes to complete a task. 

One way to do this in Scratch 3.0.

A wait block:-
  
![wait block](https://salsa.debian.org/zleap-guest/scratch3_timer/-/raw/master/wait.png)

Adds a delay (in this case one (1) second) to a program.
This is fine to control the speed of a program, what if we want to give the user some feedback, as in display the time taken on the screen.

Within Scratch if we use variables we can display information on the screen.  First we need to create a variable for our timer.

Under variables :-
Click 'Make a Variable'

![Variables](https://salsa.debian.org/zleap-guest/scratch3_timer/-/raw/master/variable-screen.png)


As we need to create a variable to hold the value time, we will call this **time.**

![make_variable](https://salsa.debian.org/zleap-guest/scratch3_timer/-/raw/master/maketimevariable.png)

Now that we have created somewhere to hold some data on time we can create, for this example a simple program to do something with it.

![final_program](https://salsa.debian.org/zleap-guest/scratch3_timer/-/raw/master/timer-final.png)

So what is going on here ?


1. Press green flag.

2. Set the Variable ‘time’ (pull down menu) to the value of 59.
3. Start loop

4. Change the value of ‘time’ by -1 (minus 1)

6. Wait for 1 second

7. go back to 4, unless the value is 0 in which case stop the loop and run any code after the loop has ended at 0.


This how_to has been created by Paul Sutton (zleap) [https://personaljournal.ca/paulsutton/](https://personaljournal.ca/paulsutton/)

Please feel free to modify and  improve,  pull requests welcome.  All graphics (other then the cc logo below) have been created, by myself with a screen shot of scratch 3.0.

Pull request back are welcome. 
Hope this resource is useful.

![cc-logo](https://salsa.debian.org/zleap-guest/scratch3_timer/-/raw/master/88x31.png)
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTc5MzU5NTQ2MF19
-->
